A boilerplate for Craft CMS project
https://bitbucket.org/codegraph/craftcms/src

Installation
-----------------------------------------------------------------
1.Clone this repository into a directory one level up of your web
 root.

2.Copy .env.exapmle to .env and configure your database
 connection

3.Move to public directory and Rename the file htaccess to
 .htaccess

4.Go to exapmle.com/admin/install for installation. Enjoy!
